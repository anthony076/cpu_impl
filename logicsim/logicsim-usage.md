## LogicSim 使用

- [舊版logicsim 下載](https://sourceforge.net/projects/circuit/files/)
- [logicsim-evolution 下載](https://github.com/logisim-evolution/logisim-evolution)

## 基本操作

- `Ctrl+1`，進入change模式，可修改元件的值，或與元件進行互動
- `Ctrl+2`，進入editor模式，修改元件的屬性
- `Ctrl+k`，觸發 CLK
- `Menu > Simulate > Time diagram`，查看時序圖

## 常用組件

- CLK
  - 位置 : `Wiring > Clock`
  - 驅動方式
    - 方法1，`Menu > Simulate > Auto-Tic-Enabled` (或 Ctrl+k 快速鍵)，自動連續觸發
    - 方法2，`Menu > Simulate > Auto-Tic-Frequency`，變更觸發的頻率
    - 方法3，`Menu > Simulate > Manual-Tic-Half-Cycle`，手動觸發半周期，0->1 或 1->0 (或 Ctrl+t 快速鍵)
    - 方法4，`Menu > Simulate > Manual-Tic-Full-Cycle`，手動觸發全周期，0->1->0 或 1->0->1 (或 F9 快速鍵)

- Pin : 用於數據的輸入和輸出
  - 位置 : `Wiring > Pin`
  - Facing : 接腳方向
  - Output : 將 pin 設置為輸出(接受輸入)
  - Data Bits : 數據長度
  - Radix : 數據類型，預設為二進制
  - 範例

    <img src="doc/pin-example.png" width=70% height=auto>

- Splitter : 
  - 位置 : `Wiring > Splitter`
  - Facing : 輸出端接腳的方向
  - Appearance : 輸入端接腳出現的位置
  - Fan Out : 輸出數據長度
    - 注意，輸出端的每一個bit都是單獨的接腳
    - 若輸入端為8bit，輸出端為2bit，則每個輸出端的接腳為 4bit 長度的數據
  - Bit Width In : 輸入數據長度，
    - 注意，輸入端的每一個bit不會單獨顯示，而是以單條粗線條的方式表示多個bit的輸入
  - 範例

    <img src="doc/spliter-example.png" width=70% height=auto>

- Display
  - 位置 : 
    - `Input/Output > 7-Segment-Display` : 需要單獨控制每一個bit，共需要控制 7+1(小數點) = 8bit
    - `Input/Output > Hex-Digit-Display` : 將要顯示的數值轉換為hex後，透過4bit控制顯示的值
  - 範例

    <img src="doc/led-example.png" width=70% height=auto>

- Adder/Subtractor 內建加法器/減法器
  - 位置 : 
    - 加法器，`Arithmetic > Adder`，含進位輸入的全加器 + 進位結果輸出，可指定輸入數據長度
    - 減法器，`Arithmetic > Subtractor`，含借位輸入的全減器 + 負數結果輸出，可指定輸入數據長度
  - 範例

    <img src="doc/adder-subtractor-example.png" width=70% height=auto>

## Examples

- 範例，查看複雜邏輯電路的真值表和公式 (自動推導)
  
  繪製邏輯閘電路後，`Project > Analyze Circuit`，LogicSim 會自動推導真值表和輸出公式

  <img src="doc/logic-circuit-analysis.png" width=70% height=auto>
 
- 範例，建立 subcircuit，變更改 subcircuit 的外觀

  <img src="doc/change-subcircuit-appearance.png" width=70% height=auto>

- 範例，獲得含時脈邏輯電路的時序圖(chronogram)

  https://www.youtube.com/watch?v=E27rFg4Zmrw

- 範例，實現 JK-FF
  - [CSCE 312: Implementing JK Flip Flop in logisim](https://www.youtube.com/watch?v=GsjvVKYJPMs)

## Resource

- Series
  - [Logism Tutorials * 57](https://www.youtube.com/playlist?list=PLFnx8Vsp0KYlkx2OIji85Z2S2ssn_zTZ4)
  
  - [CIS 221: Logisim-Evolution Labs * 14](https://www.youtube.com/playlist?list=PLvjlcTfwDj4spSN4g3S8IHbqY4Qkb5LxP)

  - [Logisim Tutorial * 18](https://www.youtube.com/playlist?list=PLFnx8Vsp0KYkhig6WCHnU-kt-1bugtl7e)
  
  - [Computer Organization and Design Lab Work * 12](https://www.youtube.com/playlist?list=PL6f7IPPkpDKlmTvZjn5MK0LsD3gpfU1nk)
  

- Single
  - [Logisim: Ripple Carry Adder](https://www.youtube.com/watch?v=DQWpjpjoy4o&list=PLFyQ8BY0EzawB2cD9FponewmC1W57mkX9)

  - [How to set up a simple subcircuit](https://www.youtube.com/watch?v=ggc0xGjmtKw)
  
  - [Using the Chronogram in Logisim](https://www.youtube.com/watch?v=E27rFg4Zmrw)
  
  - [Building an ALU in Logisim](https://www.youtube.com/watch?v=lvYCchzQTyE&list=PL7wffs-oJ6x3ey5kFlCe-qSaVsw4aHkYK)