
- [LogicCircuit 下載](https://www.logiccircuit.org/)

- [LogicSim使用教學](logicsim/logicsim-usage.md)

## 基礎電路

- Location : [MyCPU.CircuitProject](BasicCircuit/MyCPU.CircuitProject)

- manual alu

    <img src="BasicCircuit/doc/manual-alu.png" width=60% height=auto>

- auto alu

    ![image](BasicCircuit/doc/auto-alu.png)

- memory-controller (MC)

    ![image](BasicCircuit/doc/memory-controller.png)

- program-counter (PC)

    ![image](BasicCircuit/doc/program-counter.png)

- micro-program-controll

    ![image](BasicCircuit/doc/micro-program-controll.png)

- advanced-alu

    ![image](BasicCircuit/doc/advanced-alu.png)


## Build a 8 bit cpu

- Location : [8bitCPU.CircuitProject](8bitCPU/8bitCPU.CircuitProject)

- 使用方式
  - 以 LogicCircuit 開啟 8bitCPU.CircuitProject 檔案
  - 將 micro.bin 載入 ROM
  - 將 program.bin 載入 RAM
  - 點擊右下角的 RUN
  - 點擊左上角的 POW，啟動電源

- 8bit-cpu

    ![image](8bitCPU/doc/8bit-cpu.png)

- control-unit

    ![image](8bitCPU/doc/controll-unit.png)

- rw controller

    ![image](8bitCPU/doc/rw-controller.png)

- reg controller

    ![image](8bitCPU/doc/reg-controller.png)

## 其他主題
- [指令系統](8bitCPU/instruction.md)
- [記憶體管理](8bitCPU/stack、jump、function%20call、interrupt.md)
- [stack、jump、function call、interrupt的實現](8bitCPU/stack、jump、function%20call、interrupt.md)

## Ref 
- [乘法器的實現](https://www.youtube.com/watch?v=8HlLUCewXpQ)
- [減法器的實現](https://www.youtube.com/watch?v=oHlfQ_sFW0k)
- [加法器的實現](https://www.youtube.com/watch?v=EQYk2681sSA)