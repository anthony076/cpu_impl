"""
    #   從 "Counter + PC + MicroControl Demo" 的 "微程序控制器 (ROM)，控制CPU執行流程" 電路可知，
        程序控制器 ROM 控制的腳位，由 MSB 至 LSB 分別為，

    名稱    STOP    X       X       (CS_PC  EN_PC   WE_PC)   (CS_M    WE_M)    (ALU_OUT  ALU_SUB)
    位置    bit15   bit14   bit13   (bit12  bit11   bit10)   (bit9    bit8)    (bit7     bit6)
    組件                                    PC                    REG-M             ALU
    
    名稱    (CS_Z   WE_Z)   (CS_X   WE_X)   (CS_Y   WE_Y)
    位置    (bit5   bit4)   (bit3   bit2)   (bit1   bit0)
    組件        REG-Z           REG-X           REG-Y

    (可見，micro-program-controll.png)

    #   使用範例
        若要寫入 REG-Y，WE_Y=bit0=1，CS_Y=bit1=1
        若要讀取 REG-Y，WE_Y=bit0=0，CS_Y=bit1=1

        若要使用減法，ALU_SUB=bit6=1
        若要使用加法，ALU_ADD=bit6=0
        若要將RAM的資料寫入總線，CS_M=bit9=1，WE_M=bit8=0

    #   在建立指令時，需要把該指令對應的控制腳位致能，未致能的組件在該指令執行時是失效的，
        例如，將 REG-M 的資料輸出到 REG-Y，需要將 REG-M 和 REG-Y開啟
            將REG-M 的資料寫入總線，CS_M=bit9=1，WE_M=bit8=0，
            將REG-Y 致能，並開啟REG-Y的寫入功能，CS_Y=bit1=1，WE_Y=bit0=1
            指令為 0b0000 0010 0000 0011 (bit0、bit1、bit9 為1，其餘為0)

        致能沒有順序之分
            建立的指令可以是 CS_M | CS_Y | WE_Y 或 WE_Y | CS_Y | CS_M
            因為是對不同的腳位設置為1，因次順序沒有先後之分
            以上兩種都可以得到 0b0000 0010 0000 0011 的結果
"""

# 定義控制訊號
import os
from posixpath import dirname
from sys import byteorder

''' 運算邏輯相關的REG '''
# CS 使REG致能，REG致能後要執行何種功能，由 WE 決定
#   WE = 0，REG 的 DI 無效，DO 有效，資料只能從REG傳出，無法寫入
#   WE = 1，REG 的 DO 無效，DI 有效，資料只能從DI傳遞給REG，資料無法從REG傳出
WE_Y = 2**0     # 0x0000  0000  0000  0001
CS_Y = 2**1     # 0x0000  0000  0000  0010
WE_X = 2**2     # 0x0000  0000  0000  0100
CS_X = 2**3     # 0x0000  0000  0000  1000
WE_Z = 2**4     # 0x0000  0000  0001  0000
CS_Z = 2**5     # 0x0000  0000  0010  0000

''' ALU，運算邏輯單元 '''
ALU_ADD = 0
ALU_SUB = 2**6  # 0x0000  0000  0100  0000
ALU_OUT = 2**7  # 0x0000  0000  1000  0000

''' REG-M，讀取RAM/發送RAM寫入訊號/將RAM的資料寫入總線 '''
# WE_M=0 / CS_M=1，將 RAM 寫入的值，輸出到總線，
# CS=1 / WE=1 / CK=1 -> EN = 0，w = 1，發出讓RAM可被寫入資料的訊號給 RAM
WE_M = 2**8     # 0x0000  0001  0000  0000
CS_M = 2**9     # 0x0000  0010  0000  0000

''' Program-Counter '''
# 要使PC有效，CS_PC = WE_PC = 1，EN 控制+1後的位址來源，需要跳轉才需要將EN_PC設置為0
#   CS_PC，將PC內部REG的值，輸出至 AO
#   WE_PC，使 Addr 寫入PC內部的 REG
#   EN_PC，控制經過 +1 的運算後，下一個輸入內部 REG 的新位址，是 AI 或是 AO
#       EN_PC=0，將 AI 的值輸入內部REG，用於跳轉
#       EN_PC=1，將 AO 的值輸入內部REG，用於一般使用，自動讀取上一次PC+1的結果

WE_PC = 2**10   # 0x0000  0100  0000  0000
EN_PC = 2**11   # 0x0000  1000  0000  0000
CS_PC = 2**12   # 0x0001  0000  0000  0000

''' 停止執行位 '''
HAL =   2**15   # 0x1000  0000  0000  0000

def test_ram2regy():
    """
    測試從 RAM -> REGM -> REGY
    """
    print("==== test_ram2regy ====")

    print(bin(CS_M | CS_Y | WE_Y))  # 0b1000000011

    """
    0b1000000011    ->  0000 0010 0000 0011
                        0    2    0    3

    big，大頭模式，依照 MSB LSB 的順序，得到 0203
    """
    print((CS_M | CS_Y | WE_Y).to_bytes(2,byteorder="big"))  # 大頭模式，b'\x02\x03'

    """
    0b1000000011    ->  0000 0010 0000 0011
                        0    2    0    3

    little，小頭模式，將LSB放到MSB，得到 0302
    """
    print((CS_M | CS_Y | WE_Y).to_bytes(2,byteorder="little"))  # 小頭模式，b'\x03\x02'

instructions = [
    # PC 取值 > MC 從RAM 獲取數據 > 將RAM寫入總線 > 總線傳遞給 REG-Y > PC + 1
    #       WE_PC=1 / CS_PC=1 使 Program-Counter 有效，才能連續執行指令
    #       CS_M=1，將RAM的資料寫入總線
    #       CS_Y=1，將 REG-Y 致能
    #       WE_Y=1，將 總線上的資料寫入 REG-Y
    #       EN_PC=1，自動讀取上一次PC+1的結果
    WE_PC | CS_PC | CS_M | CS_Y | WE_Y | EN_PC,     # 0b0001 1110 0000 0011=0x1e03(0x031e @ little)
    
    # PC 取值 > MC 從 RAM 獲取數據 > 將RAM寫入總線 > 總線傳遞給 REG-X > PC + 1
    WE_PC | CS_PC | CS_M | CS_X | WE_X | EN_PC,     # 0b0001 1110 0000 0011=0x1e03(0x031e @ little)

    # 將 ALU 的結果輸出致能 > 將ALU結果寫入 REG-Z，
    ALU_OUT | CS_Z | WE_Z,

    # 透過 CS_M 和 WE_M 開啟寫入RAM的功能 > 將 REG-Z 的結果寫入總線 > PC+1 (選用，因為已經是指令的最後一步了)
    CS_M | WE_M | CS_Z | WE_PC | CS_PC | EN_PC,

    # 結束指令
    HAL
]

#test_ram2regy()

def main():
    dirName = os.path.dirname(__file__)
    filePath = os.path.join(dirName, 'micro-instructions.bin')

    # 在 LogicCircuit 的軟體使用 Little-Endian，因此需要以Little-Endian的方式寫入
    # 可用 notepad++ 的 hex-editor 檢查結果
    with open(filePath, 'wb') as fw:
        for ins in instructions:
            result = ins.to_bytes(2, byteorder="little")
            fw.write(result)

            print(f"writed {bin(ins)} : {result}")

if __name__ == "__main__":
    main()
