"""
    This python script will generate bellow content for 5to32 decoder rom
    00000000 00000000 00000000 00000001
    00000000 00000000 00000000 00000010
    00000000 00000000 00000000 00000100
    ....
    10000000 00000000 00000000 00000000
"""

from os.path import dirname, join

dirName = dirname(__file__)
fileName = join(dirName, "5to32_decoder_rom.bin")

with open(fileName, 'wb') as fw:
    for i in range(32):
        value = 1 << i
        result = value.to_bytes(4, byteorder="little")
        fw.write(result)
        