
"""
本檔案用於定義 Control-Unit 的輸出接腳，
當微控制程序指令透過 (d接腳@CU) 傳入 32bit的控制訊號後，控制指定REG的開關

參考，8bitCPU.CircuitProject > 00: Main > 03: Control-Unit 觀看
參考，RW值和REG的對應關係 @ doc\instruction.md
"""

class Pin:
    """
    定義 Control-Unit 的輸出接腳，

    微程序控制指令會從 ROM 輸入 Control-Unit 中，在 Control-Unit 中經過Read-Write-Control(RWC)和Registor-Control(RC)後，
    輸出使 REG 激活的控制訊號

    例如，MSR = 1，代表RC接收到的 R/W 的訊號為 0x0001，經過 5to32轉換後，會輸出 00000000 00000000 00000000 00000010，
    代表會選中位於 2^1 位置的 MSR-REG，使 MSR-REG 有效後就可以存取 Bus
    
    參考，doc/instruction.md 的 RW值和REG的對應關係
    """
    def __init__(self) -> None:
        self._DST_SHIFT = 5
        self._OP_SHIFT = 17

    @property
    def MSR(self) -> int:
        '''
        使 MSR-REG 有效 \n
        MSR = 1 (RW=0b00001)
        '''
        return 1

    @property
    def MAR(self):
        '''
        使 MAR-REG 有效 \n
        MAR = 2 (RW=0b00010)
        '''
        return 2

    @property
    def MDR(self):
        '''
        使 MDR-REG 有效 \n
        MDR = 3 (RW=0b00011)
        '''
        return 3

    @property
    def RAM(self):
        '''
        使 RAM 有效 \n
        RAM = 4 (RW=0b00100)
        '''
        return 4

    @property
    def IR(self):
        '''
        使 IR-REG 有效 \n
        IR = 5 (RW=0b00101)
        '''
        return 5

    @property
    def DST(self):
        '''
        使 DST-REG 有效 \n
        DST = 6 (RW=0b00110)
        '''
        return 6

    @property
    def SRC(self):
        '''
        使 SRC-REG 有效 \n
        SRC = 7 (RW=0b00111)
        '''
        return 7

    @property
    def A(self):
        '''
        使 A-REG 有效 \n
        A = 8 (RW=0b01000)
        '''
        return 8

    @property
    def B(self):
        '''
        使 B-REG 有效 \n
        B = 9 (RW=0b01001)
        '''
        return 9
    
    @property
    def C(self):
        '''
        使 C-REG 有效 \n
        C = 10 (RW=0b01010)
        '''
        return 10

    @property
    def D(self):
        '''
        使 D-REG 有效 \n
        D = 11 (RW=0b01011)
        '''
        return 11

    @property
    def DI(self):
        '''
        使 DI-REG 有效 \n
        DI = 12 (RW=0b01100)
        '''
        return 12

    @property
    def SI(self):
        '''
        使 SI-REG 有效 \n
        SI = 13 (RW=0b01101)
        '''
        return 13

    @property
    def SP(self):
        '''
        使 SP-REG 有效 \n
        SP = 14 (RW=0b01110)
        '''
        return 14

    @property
    def BP(self):
        '''
        使 BP-REG 有效 \n
        BP = 15 (RW=0b01111)
        '''
        return 15

    @property
    def CS(self):
        '''
        使 CS-REG 有效 \n
        CS = 16 (RW=0b10000)
        '''
        return 16

    @property
    def DS(self):
        '''
        使 DS-REG 有效 \n
        DS = 17 (RW=0b10001)
        '''
        return 17

    @property
    def SS(self):
        '''
        使 SS-REG 有效 \n
        SS = 18 (RW=0b10010)
        '''
        return 18

    @property
    def ES(self):
        '''
        使 ES-REG 有效 \n
        ES = 19 (RW=0b10011)
        '''
        return 19

    @property
    def VEC(self):
        '''
        使 VEC-REG 有效 \n
        VEC = 20 (RW=0b10100)
        '''
        return 20

    @property
    def T1(self):
        '''
        使 T1-REG 有效 \nn
        T1 = 21 (RW=10101)
        '''
        return 21

    @property
    def T2(self):
        '''
        使 T2-REG 有效 \n
        T2 = 22 (RW=10110)
        '''
        return 22

    """
    定義 32bit 微程序控制指令的 SC 位，
        - SC 值，位於32bit微程序控制指令的讀取控制位 (d[4:0] @ CU)，用於選擇要讀取的REG
        - 當 RWC 選擇SC作為REG的選擇源時，會將SC值傳遞給RW值

        - 被 SC 選擇的 REG，該REG的讀取控制接腳會被激活，此時，REG的值輸出到總線上

        - 例如，若要使用 SC 位選擇讀取 MDR 時，則完整的微程序控制指令為 xxxxxxxx xxxxxxxx xxxxxxxx xxx00011，此時 SC = 0b00011
          透過 RWC 將 RW訊號 設置為 SC 的值，此時，RC 收到的 RW值 為 0b00011，經過 RC 內部的 5to32 的轉換後，輸出 0b00000000 00000000 00000000 00001000
          位於 2^3 的MDR被選中，並激活MDR的CS接腳
    """
    @property
    def MSR_OUT(self):
        '''
        將MSR-REG的值，寫入 BUS \n
        MSR_OUT = MSR = 1 (SC = 0b00001 = 0x1)
        '''
        return self.MSR

    @property
    def MAR_OUT(self):
        '''
        將MAR-REG的值，寫入 BUS \n
        MAR_OUT = MAR = 2 (SC = 0b00010 = 0x2)
        '''
        return self.MAR

    @property
    def MDR_OUT(self):
        '''
        將MDR-REG的值，寫入 BUS \n
        MDR_OUT = MDR = 3 (SC = 0b00011 = 0x3)
        '''
        return self.MDR

    @property
    def RAM_OUT(self):
        '''
        將RAM的值，寫入 BUS \n
        RAM_OUT = RAM = 4 (SC = 0b00100 = 0x4)
        '''
        return self.RAM

    @property
    def IR_OUT(self):
        '''
        將IR-REG的值，寫入 BUS \n
        IR_OUT = IR = 5 (SC = 0b00101 = 0x5)
        '''
        return self.IR

    @property
    def DST_OUT(self):
        '''
        將DST-REG的值，寫入 BUS \n
        DST_OUT = DST = 6 (SC = 0b00110 = 0x6)
        '''
        return self.DST

    @property
    def SRC_OUT(self):
        '''
        將SRC-REG的值，寫入 BUS \n
        SRC_OUT = SRC = 7 (SC = 0b00111 = 0x7)
        '''
        return self.SRC

    @property
    def A_OUT(self):
        '''
        將A-REG的值，寫入 BUS \n
        A_OUT = A = 8 (SC = 0b01000 = 0x8)
        '''
        return self.A

    @property
    def B_OUT(self):
        '''
        將B-REG的值，寫入 BUS \n
        B_OUT = B = 9 (SC = 0b01001 = 0x9)
        '''
        return self.B

    @property
    def C_OUT(self):
        '''
        將C-REG的值，寫入 BUS \n
        C_OUT = C = 10 (SC = 0b01010 = 0xa)
        '''
        return self.C

    @property
    def D_OUT(self):
        '''
        將D-REG的值，寫入 BUS \n
        D_OUT = D = 11 (SC = 0b01011 = 0xb)
        '''
        return self.D

    @property
    def DI_OUT(self):
        '''
        將DI-REG的值，寫入 BUS \n
        DI_OUT = DI = 12 (SC = 0b01100 = 0xc)
        '''
        return self.DI

    @property
    def SI_OUT(self):
        '''
        將SI-REG的值，寫入 BUS \n
        SI_OUT = SI = 13 (SC = 0b01101 = 0xd)
        '''
        return self.SI

    @property
    def SP_OUT(self):
        '''
        將SP-REG的值，寫入 BUS \n
        SP_OUT = SP = 14 (SC = 0b01110 = 0xe)
        '''
        return self.SP

    @property
    def BP_OUT(self):
        '''
        將BP-REG的值，寫入 BUS \n
        BP_OUT = BP = 15 (SC = 0b01111 = 0xf)
        '''
        return self.BP

    @property
    def CS_OUT(self):
        '''
        將CS-REG的值，寫入 BUS \n
        CS_OUT = CS = 16 (SC = 0b10000 = 0x10)
        '''
        return self.CS

    @property
    def DS_OUT(self):
        '''
        將DS-REG的值，寫入 BUS \n
        DS_OUT = DS = 17 (SC = 0b10001 = 0x11)
        '''
        return self.DS

    @property
    def SS_OUT(self):
        '''
        將SS-REG的值，寫入 BUS \n
        SS_OUT = SS = 18 (SC = 0b10010 = 0x12)
        '''
        return self.SS

    @property
    def ES_OUT(self):
        '''
        將ES-REG的值，寫入 BUS \n
        ES_OUT = ES = 19 (SC = 0b10011 = 0x13)
        '''
        return self.ES

    @property
    def VEC_OUT(self):
        '''
        將VEC-REG的值，寫入 BUS \n
        VEC_OUT = VEC = 20 (SC = 0b10100 = 0x14)
        '''
        return self.VEC

    @property
    def T1_OUT(self):
        '''
        將T1-REG的值，寫入 BUS \n
        T1_OUT = T1 = 21 (SC = 0b10101 = 0x15)
        '''
        return self.T1

    @property
    def T2_OUT(self):
        '''
        將T2-REG的值，寫入 BUS \n
        T2_OUT = T2 = 22 (SC = 0b10110 = 0x16)
        '''
        return self.T2

    """
    定義 32bit 微程序控制指令的 DC 位，
        - DC 值，位於32bit微程序控制指令的寫入控制位 (d[9:5] @ CU)，用於選擇要寫入的REG
        - 當 RWC 選擇DC作為REG的選擇源時，會將DC值處理後，再傳遞給RW值
          DC 值位於 32bit 的 第 5-9 位，因此，需要將 RW 值左移5位後，才是正確的 DC 值

        - 被 DC 選擇的 REG，該REG的寫入控制接腳會被激活，此時，總線上的數據，會被寫入到REG中

        - 例如，若要使用 DC 位選擇寫入 MDR 時，微程序控制指令為 xxxxxxxx xxxxxxxx xxxxxx00 011xxxxx，此時 DS = 00011
          透過 RWC 將RW訊號 設置為 DC 的值，此時，RC 收到的RW值 為 0b00011，經過 RC 內部的 5to32 的轉換後，輸出 0b00000000 00000000 00000000 00001000
          位於 2^3 的MDR被選中，並激活MDR的WE接腳
    """

    @property
    def MSR_IN(self):
        '''
        將 BUS 上的值，寫入 MSR-REG \n
        MSR_IN = MSR << _DST_SHIFT = 0b00001 << 5 = 0b100000 = 0x20 (DC=0x20)
        '''
        return self.MSR << self._DST_SHIFT

    @property
    def MAR_IN(self):
        '''
        將 BUS 上的值，寫入 MAR-REG \n
        MAR_IN = MAR << _DST_SHIFT = 0b00010 << 5 = 0b1000000 = 0x40 (DC=0x40)
        '''
        return self.MAR << self._DST_SHIFT

    @property
    def MDR_IN(self):
        '''
        將 BUS 上的值，寫入 MDR-REG \n
        MDR_IN = MDR << _DST_SHIFT = 0b00011 << 5 = 0b1100000 = 0x60 (DC=0x60)
        '''
        return self.MDR << self._DST_SHIFT

    @property
    def RAM_IN(self):
        '''
        將 BUS 上的值，寫入 RAM \n
        RAM_IN = RAM << _DST_SHIFT = 0b10000000 = 0x80 (DC=0x80)
        '''
        
        return self.RAM << self._DST_SHIFT
        
    @property
    def IR_IN(self):
        '''
        將 BUS 上的值，寫入 IR-REG \n
        IR_IN = IR << _DST_SHIFT = 0b10100000 = 0xa0 (DC=0xa0)
        '''
        return self.IR << self._DST_SHIFT

    @property
    def DST_IN(self):
        '''
        將 BUS 上的值，寫入 DST-REG \n
        DST_IN = DST << _DST_SHIFT = 0b11000000 = 0xc0 (DC=0xc0)
        '''
        return self.DST << self._DST_SHIFT

    @property
    def SRC_IN(self):
        '''
        將 BUS 上的值，寫入 SRC-REG \n
        SRC_IN = SRC << _DST_SHIFT = 0b11100000 = 0xe0 (DC=0xe0)
        '''
        return self.SRC << self._DST_SHIFT

    @property
    def A_IN(self):
        '''
        將 BUS 上的值，寫入 A-REG \n
        A_IN = A << _DST_SHIFT = 0b100000000 = 0x100 (DC=0x100)
        '''
        return self.A << self._DST_SHIFT

    @property
    def B_IN(self):
        '''
        將 BUS 上的值，寫入 B-REG \n
        B_IN = B << _DST_SHIFT = 0b100100000 = 0x120 (DC=0x120)
        '''
        return self.B << self._DST_SHIFT

    @property
    def C_IN(self):
        '''
        將 BUS 上的值，寫入 C-REG \n
        C_IN = C << _DST_SHIFT = 0b101000000 = 0x140 (DC=0x140)
        '''
        return self.C << self._DST_SHIFT

    @property
    def D_IN(self):
        '''
        將 BUS 上的值，寫入 D-REG \n
        D_IN = D << _DST_SHIFT = 0b101100000 = 0x160 (DC=0x160)
        '''
        return self.D << self._DST_SHIFT

    @property
    def DI_IN(self):
        '''
        將 BUS 上的值，寫入 DI-REG \n
        DI_IN = DI_IN << _DST_SHIFT = 0b110000000 = 0x180 (DC=0x180)
        '''
        return self.DI << self._DST_SHIFT

    @property
    def SI_IN(self):
        '''
        將 BUS 上的值，寫入 SI-REG \n
        SI_IN = SI << _DST_SHIFT = 0b110100000 = 0x1a0 (DC=0x1a0)
        '''
        return self.SI << self._DST_SHIFT

    @property
    def SP_IN(self):
        '''
        將 BUS 上的值，寫入 SP-REG \n
        SP_IN = SP << _DST_SHIFT = 0b111000000 = 0x1c0 (DC=0x1c0)
        '''
        return self.SP << self._DST_SHIFT

    @property
    def BP_IN(self):
        '''
        將 BUS 上的值，寫入 BP-REG \n
        BP_IN = BP << _DST_SHIFT = 0b111100000 = 0x1e0 (DC=0x1e0)
        '''
        return self.BP << self._DST_SHIFT

    @property
    def CS_IN(self):
        '''
        將 BUS 上的值，寫入 CS-REG \n
        CS_IN = CS << _DST_SHIFT = 0b1000000000 = 0x200 (DC=0x200)
        '''
        return self.CS << self._DST_SHIFT

    @property
    def DS_IN(self):
        '''
        將 BUS 上的值，寫入 DS-REG \n
        DS_IN = DS << _DST_SHIFT = 0b1000100000 = 0x220 (DC=0x220)
        '''
        return self.DS << self._DST_SHIFT

    @property
    def SS_IN(self):
        '''
        將 BUS 上的值，寫入 SS-REG \n
        SS_IN = SS << _DST_SHIFT = 0b1001000000 = 0x240 (DC=0x240)
        '''
        return self.SS << self._DST_SHIFT

    @property
    def ES_IN(self):
        '''
        將 BUS 上的值，寫入 ES-REG \n
        ES_IN = ES << _DST_SHIFT = 0b1001100000 = 0x260 (DC=0x260)
        ''' 
        return self.ES << self._DST_SHIFT

    @property
    def VEC_IN(self):
        '''
        將 BUS 上的值，寫入 VEC-REG \n
        VEC_IN = VEC << _DST_SHIFT = 0b1010000000 = 0x280 (DC=0x280)
        '''
        return self.VEC << self._DST_SHIFT

    @property
    def T1_IN(self):
        '''
        將 BUS 上的值，寫入 T1-REG \n
        T1_IN = T1 << _DST_SHIFT = 0b1010100000 = 0x2a0 (DC=0x2a0)
        '''
        return self.T1 << self._DST_SHIFT

    @property
    def T2_IN(self):
        '''
        將 BUS 上的值，寫入 T2-REG \n
        T2_IN = T2 << _DST_SHIFT = 0b1011000000 = 0x2c0 (DC=0x2c0)
        '''
        return self.T2 << self._DST_SHIFT


    """
    定義 32bit 微程序控制指令的 SR、SW、DR、DW 控制位，
    控制從 DST-REG(D-In腳位) 或 SRC-REG(S-In腳位) 讀取RW的控制訊號
    """
    @property
    def SRC_R(self):
        '''
        SR=1，將 SRT-REG 的值，指向要讀取的 REG，例如，SRT=A，讀取第 10 個 REG (讀取 C-REG) \n
        SRC_R = 2 ** 10 = 0b0000 0000 0000 0000 0000 0100 0000 0000 = 0x400
        '''
        return 2 ** 10

    @property
    def SRC_W(self):
        '''
        SW=1，將 SRT-REG 的值，指向要寫入的 REG，例如，SRT=A，寫入第 10 個 REG (寫入 C-REG) \n
        SRC_R = 2 ** 11 = 0b0000 0000 0000 0000 0000 1000 0000 0000 = 0x800
        '''
        return 2 ** 11

    @property
    def DST_R(self):
        '''
        DR=1，將 DST-REG 的值，指向要讀取的 REG，例如，DST=A，讀取第 10 個 REG (讀取 C-REG) \n
        SRC_R = 2 ** 12 = 0b0000 0000 0000 0000 0001 0000 0000 0000 = 0x1000
        '''
        return 2 ** 12
        
    @property
    def DST_W(self):
        '''
        DW=1，將 DST-REG 的值，指向要寫入的 REG，例如，DST=A，寫入第 10 個 REG (寫入 C-REG) \n
        SRC_R = 2 ** 13 =  0b0000 0000 0000 0000 0010 0000 0000 0000 = 0x2000
        '''
        return 2 ** 13

    """
    定義 32bit 微程序控制指令的 PC 控制位 和 PC 相關功能
    """
    @property
    def PC_WE(self):
        '''
        PC_WE，將新的位址寫入PC \n
        PC_WE = 2 ** 14 = 0b0000 0000 0000 0000 0100 0000 0000 0000 = 0x4000
        '''
        return 2 ** 14

    @property
    def PC_CS(self):
        '''
        PC_CS，使 PC 有效 \n
        PC_CS = 2 ** 15 = 0b0000 0000 0000 0000 1000 0000 0000 0000 = 0x8000
        '''
        return 2 ** 15

    @property
    def PC_EN(self):
        '''
        PC_EN，選擇 PC 的輸入源，EN=1，從前一次的結果輸入輸入新地址，EN=0，從 DI 輸入新地址 \n
        PC_EN = 2 ** 16 = 0b0000 0000 0000 0001 0000 0000 0000 0000 = 0x10000
        '''
        return 2 ** 16
        
    @property
    def PC_OUT(self):
        '''
        使 PC 的值輸出到 BUS \n
        PC_OUT = PC_CS = 0b0000 0000 0000 0000 1000 0000 0000 0000 = 0x8000
        '''
        return self.PC_CS

    @property
    def PC_IN(self):
        '''
        將 BUS 上的值輸入 PC \n
        PC_IN = PC_CS | PC_WE = 0b0000 0000 0000 0000 1100 0000 0000 0000 = 0xc000
        '''
        return self.PC_CS | self.PC_WE

    @property
    def PC_INC(self):
        '''
        執行 PC+1 \n
        PC_INC = PC_CS | PC_WE | PC_EN = 0b0000 0000 0000 0001 1100 0000 0000 0000 = 0x1c000
        '''
        return self.PC_CS | self.PC_WE | self.PC_EN
    
    """
    定義 ALU 相關控制位
    """
    @property
    def OP_ADD(self) -> int:
        '''
        將 ALU 的OP設置為0(OP=0)，使ALU的加法器生效
        OP_ADD = 0 << 17 = 0
        '''
        return 0
    @property
    def OP_SUB(self) -> int:
        '''
        將 ALU 的OP設置為1(OP=1)，使ALU的減法器生效 \n 
        OP在電路中位於 bit[19:17]，因此需要左移 17 位 \n
        OP_SUB = 1 << 17 = 0x20000
        '''
        return 1 << self._OP_SHIFT
    @property
    def OP_INC(self) -> int:
        '''
        將 ALU 的OP設置為2(OP=2)，使ALU的+1器生效 \n
        OP在電路中位於 bit[19:17]，因此需要左移 17 位 \n
        OP_INC = 2 << 17 = 0x40000
        '''
        return 2 << self._OP_SHIFT
    @property
    def OP_DEC(self) -> int:
        '''
        將 ALU 的OP設置為3(OP=3)，使ALU的-1器生效 \n
        OP在電路中位於 bit[19:17]，因此需要左移 17 位 \n
        OP_DEC = 3 << 17 = 0x60000
        '''
        return 3 << self._OP_SHIFT
    @property
    def OP_AND(self) -> int:
        '''
        將 ALU 的OP設置為4(OP=4)，使ALU的AND器生效 \n
        OP在電路中位於 bit[19:17]，因此需要左移 17 位 \n
        OP_AND = 4 << 17 = 0x80000
        '''
        return 4 << self._OP_SHIFT
    @property
    def OP_OR(self) -> int:
        '''
        將 ALU 的OP設置為5(OP=5)，使ALU的OR器生效 \n
        OP在電路中位於 bit[19:17]，因此需要左移 17 位 \n
        OP_OR = 5 << 17 = 0xa0000
        '''
        return 5 << self._OP_SHIFT
    @property
    def OP_XOR(self) -> int:
        '''
        將 ALU 的OP設置為6(OP=6)，使ALU的XOR器生效 \n
        OP在電路中位於 bit[19:17]，因此需要左移 17 位 \n
        OP_XOR = 6 << 17 = 0xc0000
        '''
        return 6 << self._OP_SHIFT
    @property
    def OP_NOT(self) -> int:
        '''
        將 ALU 的OP設置為7(OP=7)，使ALU的NOT器生效 \n
        OP在電路中位於 bit[19:17]，因此需要左移 17 位 \n
        OP_NOT = 7 << 17 = 0xe0000
        '''
        return 7 << self._OP_SHIFT

    @property
    def ALU_OUT(self) -> int:
        '''
        ALU 輸出致能控制位
        ALU_OUT = 1 << 20 = 0x100000
        '''
        return 1 << 20

    @property
    def ALU_PSW(self) -> int:
        '''
        ALU PSW 結果輸出致能控制位 \n
        ALU_PSW = 1 << 21 = 0x200000
        '''
        return 1 << 21

    """
    定義 32bit 微程序控制指令的其他控制位
    """
    @property
    def CYC(self):
        '''
        將 32bit 微程序控制指令的CYC位(bit30)設置為1 \n
        CYC 是在Control-Unit的內部，透過 ROM 控制微程序控制週期計數器(PC)的EN、CS、WE接腳\n
            CYC = 0，ROM 輸出 0x7=0b111 (EN=CS=WE=1)，執行 pc + 1 \n
            CYC = 1，ROM 輸出 0x3=0b011 (EN=0，CS=WE=1)，執行 PC 清除為零\n
        CYC = 2 ** 30 = 0b0100 0000 0000 0000 0000 0000 0000 0000 = 0x40000000
        '''
        return 2 ** 30

    @property
    def HLT(self):
        '''
        32bit 微程序控制指令的結束位(bit31)，代表指令的結束 \n
        HLT = 2 ** 31 = 0b1000 0000 0000 0000 0000 0000 0000 0000 = 0x80000000
        '''
        return 2 ** 31

    """
    定義 ir 指令格式
    """
    @property
    def ADDR2(self):
        '''
        2地址指令的標誌位，需要2個操作數的指令為2地址指令，例如，MOV A B \n
        格式，1xxx[aa][bb]，ADDR2 會將指令的 bit7 最高位設置為1 \n
        ADDR2 = 1 << 7 = 0x80 = 0b1000 0000 = 0d128
        '''
        return 1<<7

    @property
    def ADDR1(self):
        '''
        1地址指令的偏移量，需要1個操作數的指令為1地址指令，例如，INC 6 \n
        格式，01xxxx[aa]，ADDR1 會將指令的 bit6 最高位設置為1 \n
        ADDR1 = 1 << 6 = 0x40 = 0b0100 0000 = 0d64
        '''
        return 1<<6

    @property
    def ADDR2_SHIFT(self):
        '''
        2地址指令的偏移量，需要2個操作數的指令為2地址指令，例如，MOV A B \n
        格式，1xxx[aa][bb]，xxx 代表指令，位於bit[6:4]，因此需要將指令左移4位 \n
        ADDR2_SHIFT = 4
        '''
        return 4

    @property
    def ADDR1_SHIFT(self):
        '''
        1地址指令的偏移量，需要1個操作數的指令為1地址指令，例如，INC 6 \n
        格式，01xxxx[aa]，xxxx 代表指令，位於bit[5:2]，因此需要將指令左移2位 \n
        ADDR1_SHIFT = 2
        '''
        return 2

    """
    定義 4 種循址方式
    """
    @property
    def AM_INS(self):
        '''
        AM_INS = 0 \n
        Address-Method-Instance 立即循址，從立即數取得數據 \n
        設置立即循址的編碼(AM_INS) = 0
        '''
        return 0

    @property
    def AM_REG(self):
        '''
        AM_REG = 1 \n
        Address-Method-Register 寄存器循址，從暫存器取得數據 \n
        設置寄存器循址的編碼(AM_REG) = 1
        '''
        return 1

    @property
    def AM_DIR(self):
        '''
        AM_DIR = 2 \n
        Address-Method-Direct 直接循址，從記憶體取得數據 \n
        設置直接循址的編碼(AM_DIR) = 2
        '''
        return 2

    @property
    def AM_RAM(self):
        '''
        AM_RAM = 3 \n
        Address-Method-RAM 寄存器間接循址，將寄存器作為記憶體地址 \n
        設置寄存器間接循址的編碼(AM_RAM) = 3
        '''
        return 3

    """
    中斷相關控制
    """
    @property
    def ALU_INT_W(self) -> int:
        '''
        中斷控制致能位，位於 alu[2] \n
        ALU_INT_W = alu[2] = 1，中斷允許狀態可以由 alu[3] 變更 \n
        ALU_INT_W = 1 << 22 = 0x400000
        '''
        return 1 << 22

    @property
    def ALU_INT(self) -> int:
        '''
        中斷允許狀態變更位，位於 alu[3]，用於設置psw的中斷允許狀態(psw[3])的值 \n
        注意，必須 ALU_INT_W = 1，才能進行 ALU_INT 對中斷允許狀態的變更 \n
        ALU_INT_W=1，ALU_INT = psw[3] = 0，經反向後，psw[3] = 1，允許中斷 \n 
        ALU_INT_W=1，ALU_INT = psw[3] = 1，經反向後，psw[3] = 0，不允許中斷 \n
        ALU_INT = 1 << 23 = 0x800000
        '''
        return 1 << 23

    @property
    def ALU_STI(self) -> int:
        '''
        STI，Set Interrupt，開啟中斷 \n
        ALU_STI，是開啟中斷的高階封裝
        ALU_STI = ALU_INT_W = 0x400000
        '''
        return self.ALU_INT_W

    @property
    def ALU_CLI(self) -> int:
        '''
        CLI，Close Interrupt，關閉中斷 \n
        ALU_CLI，是關閉中斷的高階封裝 \n
        ALU_CLI = ALU_INT_W | ALU_INT = 0xc00000
        '''
        return self.ALU_INT_W | self.ALU_INT



pin = Pin()