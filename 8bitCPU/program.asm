; ===== TEST-2 ====
    MOV SS, 1
    MOV SP, 0x20
    JMP start

show:
    ; 內中斷函數，顯示 255
    MOV D, 255
    iret

start:
    MOV C, 0

increase:
    ; +1 後判斷奇偶數，
    ;   若奇，跳到 disable
    ;   若偶，繼續往下執行
    INC C
    MOV D, C
    JP disable

enable:
    ; 偶數區，開啟中斷允許
    STI
    JMP interrupt

disable:
    ; 奇數區，關閉中斷允許，關閉後繼續往下執行
    CLI

interrupt:
    ; 只對偶數有效，進入中斷函數，顯示 255
    INT show

    JMP increase

    HLT

;; ===== TEST-1 ====
;     MOV SS, 1
;     MOV SP, 0x20
;     JMP start

; show:
;     MOV D, 255
;     iret

; start:
;     MOV C, 0

; increase:
;     INC C
;     MOV D, C
;     CLI ; 關閉中斷允許，關閉後不會顯示 255，只會計數
;     INT show
;     JMP increase

;     HLT