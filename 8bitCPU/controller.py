"""
controller.py 用於建立微程序控制指令 (micro.bin)
"""
import os

from pin_define import pin
import assembly as ASM

dirName = os.path.dirname(__file__)
fileName = os.path.join(dirName, "micro.bin")

# 將所有位置都填入 0x8000
micro = [pin.HLT for _ in range(0x10000)]

CJMPS = [ASM.JO, ASM.JNO, ASM.JZ, ASM.JNZ, ASM.JP, ASM.JNP]

def compile_addr2(addr, ir, psw, index):
    """
    - 以 MOV A 30 = 0x8406 為例，addr = 0x8406 = 0b1000 0100 0000 0110 (16bit)

      1000 0100 0000 0110              詳見 微程序控制指令基礎 > ROM 記憶體位址 (16bit) 的編碼 @ instruction.md
                     ^^^^ cyc
                ^^^^      psw
      ^^^^^^^^^           ir

    - 其中，ir = 0x1000 0100 的組成元素

      1000 0100                        詳見，微程序控制指令基礎 > ir指令 (8bit) 的編碼 @ instruction.md
      ^         2地址指令的標記位   
       ^^^      MOV 的二進制編碼       (以下的 op)
           ^^   第1個操作數的循址方式   (以下的amd，address-mode-dst)
             ^^ 第2個操作數的循址方式   (以下的ams，address-mode-src)
    """
    
    global micro

    # 因為2地址指令，1xxx[aa][bb]，若要取出 ir，需要保留前4位
    # 因此需要將 ir & 0xf0
    op = ir & 0xf0

    # 從 ir 取出dst
    amd = (ir >> 2) & 3

    # 從 ir 取出src
    ams = ir & 3

    # ==== 檢查 op 是否有效 ====
    INST = ASM.INSTRUCTIONS[2]
    if op not in INST:
        micro[addr] = pin.CYC
        return
    
    # ==== 檢查 操作數 是否有效 ====
    am = (amd, ams)
    if am not in INST[op]:
        micro[addr] = pin.CYC
        return

    # ==== 插入 ir指令對應的控制訊號 ====
    # 在扣除 fetch 指令後，是否還有足夠的微指令空間可供 op 執行
    # 例如，一個微程序控制指令微 16 個儲存空間，扣除fetch占用的6個儲存空間，剩下10個儲存空間
    # 若要執行的 ir 空間 > 10，則空間無法插入
    EXEC = INST[op][am]
    if index < len(EXEC):
        # 空間足夠，將 ir指令對應的控制訊號插入
        micro[addr] = EXEC[index]
    else:
        # 空間不足
        micro[addr] = pin.CYC

def get_condition_jump(exec, op, psw):
    """
    get_condition_jump()，用於判斷跳轉指令的寫入時機，
    需要同時符合，1_op 是跳轉指令，2_當前位址的PSW的狀態符合跳轉指令 時
    才將跳轉指令的控制訊號寫入，否則寫入 pin.CYC

    psw[0] = 溢出位，psw[1]] = 零位，psw[2] = 奇偶位
    詳見，12: ALU - simplify layout @ 8bitCPU.CircuitProject
    """
    # 判斷是否產生溢位，只有 psw 的 bit0 為1，overflow 才為 1，否則為 0
    overflow = psw & 1
    # 判斷是否產生零位，只有 psw 的 bit1 為1，zero 才為 1，否則為 0
    zero = psw & 2
    # 判斷是否產生奇位，只有 psw 的 bit2 為1，parity 才為 1，否則為 0
    parity = psw & 4

    # 當 op == ASM.JO 且產生溢位的記憶體位置，才寫入JO的控制訊號
    if (op == ASM.JO) and overflow:
        return exec
    # 當 op == ASM.JNO 且不產生溢位的記憶體位置，才寫入JNO控制訊號
    if (op == ASM.JNO) and (not overflow):
        return exec

    if (op == ASM.JZ) and zero:
        return exec
    if (op == ASM.JNZ) and (not zero):
        return exec

    if (op == ASM.JP) and parity:
        return exec
    if (op == ASM.JNP) and (not parity):
        return exec

    # op 正確，但不符合PSW條件的記憶體位置，寫入 pin.CYC 的控制訊號
    return [pin.CYC]

def get_interrupt(exec, op, psw):
    """
    類似 get_condition_jump()，因為 INT 需要一樣使用 PSW 進行判斷
    因此透過 get_interrupt() 判斷 中斷指令的寫入時機

    需要同時符合，1_op 是中斷指令，2_當前位址的PSW的狀態符合中斷指令時
    才將中斷指令的控制訊號寫入，否則寫入 pin.CYC

    psw[3] = 中斷允許狀態位
    詳見，12: ALU - simplify layout @ 8bitCPU.CircuitProject
    """
    # 判斷中斷允許狀態是否為1，只有 psw 的 bit3 為1，interrupt 才為 1，否則為 0
    interrupt = psw & 0b1000

    # 當 op == ASM.JO 且產生溢位的記憶體位置，才寫入JO的控制訊號
    if interrupt:
        return exec

    # op 正確，但不符合PSW條件的記憶體位置，寫入 pin.CYC 的控制訊號
    return [pin.CYC]

def compile_addr1(addr, ir, psw, index):

    global micro
    global CJMPS

    # 從 ir 取出op 
    # 因為1地址指令，01xxxx[aa]，若要取出 ir，需要保留前6位
    # 因此需要將 ir & 0xfc
    op = ir & 0xfc  

    # 從 ir 取出dst
    amd = ir & 3

    # ==== 檢查 op 是否有效 ====
    INST = ASM.INSTRUCTIONS[1]
    if op not in INST:
        micro[addr] = pin.CYC
        return
    
    # ==== 檢查 操作數 是否有效 ====
    
    if amd not in INST[op]:
        micro[addr] = pin.CYC
        return

    # ==== 插入 ir指令對應的控制訊號 ====
    # 在扣除 fetch 指令後，是否還有足夠的微指令空間可供 op 執行
    # 例如，一個微程序控制指令微 16 個儲存空間，扣除fetch占用的6個儲存空間，剩下10個儲存空間
    # 若要執行的 ir 空間 > 10，則空間無法插入
    EXEC = INST[op][amd]

    # 若是跳轉指令，調用 get_condition_jump()判斷PSW的狀態是否符合
    # 若符合才將 跳轉指令的控制訊號寫入
    if op in CJMPS:
        EXEC = get_condition_jump(EXEC, op, psw)

    # 若是中斷指令，調用 get_interrupt()判斷PSW的狀態是否符合
    # 若符合才將中斷指令的控制訊號寫入
    if op == ASM.INT:
        EXEC = get_interrupt(EXEC, op, psw)

    if index < len(EXEC):
        # 空間足夠，將 ir指令對應的控制訊號插入
        micro[addr] = EXEC[index]
    else:
        # 空間不足
        micro[addr] = pin.CYC

def compile_addr0(addr, ir, psw, index):
    global micro

    # 從 ir 取出 ALU 操作，例如 HLT、NOP
    op = ir 

    # 檢查 op 是否有效
    INST = ASM.INSTRUCTIONS[0]
    if op not in INST:
        micro[addr] = pin.CYC
        return

    # 判斷當前 16bit 的微程序控制指令
    # 在扣除 fetch 指令後，是否還有足夠的微指令空間可供 op 執行
    EXEC = INST[op]
    if index < len(EXEC):
        # 空間足夠
        micro[addr] = EXEC[index]
    else:
        # 空間不足
        micro[addr] = pin.CYC

def makeMicro():
    """
    -   addr 是 ir指令的記憶體位址編碼，用來存取ir指令對應的控制訊號
        addr = IR[15:8]psw[7:4]cyc[3:0]，共 16bit
        詳見 微程序控制指令基礎 > ROM 記憶體位址 (16bit) 的編碼 @ instruction.md

    -   因為 ROM 是 16 * 32，代表每一個微程序控制指令，可以由 16個微指令所組成

    -   cyc 代表一個微指令週期，
        cyc & 0xf，只會保留低四位，因此，超過 15 後，會被重置為 0，因此，cyc 的範圍只會從 0 - 15 循環
    """
    for addr in range(0x10000):
        ir = addr >> 8
        psw = (addr >> 4) & 0xf
        cyc = addr & 0xf               
        
        # 每個微程序控制指令的第一步，都是透過 fetch 取回 ir 指令
        if cyc < len(ASM.FETCH):
            # 操作未完成，將當前fetch的操作，寫入 micro 中
            micro[addr] = ASM.FETCH[cyc]
            continue
        
        # 判斷 ir 指令的類型
        addr2 = ir & (1<<7)
        addr1 = ir & (1<<6)

        # index 代表當前微程序控制指令的第 index 個微指令
        #   cyc 會 0-15 循環，0-5 是寫入 fetch，到此步驟時 cyc = 6
        #   例如，index = cyc - len(ASM.FETCH) = 0，
        #   cyc 最大值 15-6 = 9，因此， index 最多指向 9 個微指令
        index = cyc - len(ASM.FETCH)

        if addr2 :
            compile_addr2(addr, ir, psw, index)
        elif addr1:
            compile_addr1(addr, ir, psw, index)
        else:
            compile_addr0(addr, ir, psw, index)

def micro2bin():
    with open(fileName, 'wb') as fw:
        for var in micro:
            value = var.to_bytes(4, byteorder="little")
            fw.write(value)

makeMicro()
micro2bin()

print("Compile micro instruction finished")