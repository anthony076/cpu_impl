"""
compiler.py，用於將 program.asm 編譯為 program.bin
program.bin 的內容為放在RAM中的ir指令
"""

import os
import re
#from sys import byteorder
from typing import List, Tuple

from pin_define import pin
import assembly as ASM

dirName = os.path.dirname(__file__)

inputFile = os.path.join(dirName, "program.asm")
outputFile = os.path.join(dirName, "program.bin")

# 用於存放代碼
codes = []
marks = {}

"""
定義 ir指令和二進制碼之間的映射
"""
OP2 = {
    'MOV' : ASM.MOV,    # ASM.MOV = 0x80
    'ADD' : ASM.ADD,
    'SUB' : ASM.SUB,
    'CMP' : ASM.CMP,
    'AND' : ASM.AND,
    'OR' : ASM.OR,
    'XOR' : ASM.XOR,
}

OP1 = {
    "INC" : ASM.INC,
    "DEC" : ASM.DEC,
    "NOT" : ASM.NOT,
    "JMP" : ASM.JMP,
    "JO" : ASM.JO,
    "JNO" : ASM.JNO,
    "JZ" : ASM.JZ,
    "JNZ" : ASM.JNZ,
    "JP" : ASM.JP,
    "JNP" : ASM.JNP,
    "PUSH" : ASM.PUSH,
    "POP" : ASM.POP,
    "CALL" : ASM.CALL,
    "INT" : ASM.INT,

}

OP0 = {
    'NOP' : ASM.NOP,
    'HLT' : ASM.HLT,
    'RET' : ASM.RET,
    "IRET": ASM.IRET,
    "STI" : ASM.STI,
    "CLI" : ASM.CLI,
}

"""
取得指令對應的編碼值
"""
OP2SET = set(OP2.values())
OP1SET = set(OP1.values())
OP0SET = set(OP0.values())

"""
定義可操作的REG
"""
REGISTERS = {
    'A' : pin.A,
    'B' : pin.B,
    'C' : pin.C,
    'D' : pin.D,
    'SS' : pin.SS,
    'SP' : pin.SP,
    'CS' : pin.CS,
}

"""
re express
"""
re_annotation = re.compile(r"(.*?);.*")

class Code:
    TYPE_CODE = 1
    TYPE_LABEL = 2

    def __init__(self, number:int, source:str) -> None:
        """
        @ parameter: number 行號
        @ parameter: source 原始代碼
        """
        # 原始代碼的行號
        self.number = number
        self.source = source.upper()
        self.op = None
        self.dst = None
        self.src = None
        self.type = self.TYPE_CODE
        
        # 在 codes 中，經過標記處理後的 index，
        # 用於 get_am() 中，決定JMP跳轉的位置
        self.index = 0  

        self.prepare_source()

    """
    取得 op 的二進制編碼
    從 self.op 取得指令對應的二進制編碼
    例如，self.op = MOV, 取得 0x80
    """
    def get_op(self) -> int:
        if self.op in OP2:
            return OP2[self.op]

        if self.op in OP1:
            return OP1[self.op]

        if self.op in OP0:
            return OP0[self.op]

        raise SyntaxError(self)

    """
    將 dst、src 的位置，轉譯為二進制編碼
        從循址模式 (addrMode) 取得循址模式的編碼，和對應的微程序控制訊號
        例如，addrMode 是 a，就返回 REG-A 的微程序控制訊號
        例如，addrMode 是 123，就直接返回 123
        返回 (當前循址模式的編碼, 循址模式對應的控制訊號或直接返回值)
    """
    def get_am(self, addrMode:str) -> Tuple[int, int]:
        global marks
        
        # addrMode = null
        if not addrMode:
            return None, None

        # 處理跳轉標記
        if addrMode in marks:
            """
            例如，遇到 JMP increate 的指令時，傳入的 addrMode = increate 且 marks[increate] = 1，
            代表遇到 JMP increase 指令時，要回到 increase 標記的下一行 
            (要回到 index=1 的指令行，ADD D, 50 的指令行，見下圖)

            ADD D, 50 的指令行在 codes 的 index = 1，在 RAM 位於 bit3 的位置 (見下圖)
            因此，要回傳的立即數 = marks[addrMode].index * 3 = 3，
            代表 get_am() 會將 increate 以 0x3 取代

                            ir dst src
            MOV D, 1        84, 0b, 01  占用 3byte      index0
            increase:       
            ADD D, 50       94, 0b, 32  占用 3byte      index1
            JMP increase    4c, 03, 00  占用 3byte      index2  ，控制訊號 0xc006 (DST_OUT | PC_IN)，會將 03 的值寫入PC後
                                                                ，PC -> MAR 將記憶體指向 3，回到第二個指令(index=1)
            HLT             3f, 00, 00  占用 3byte      index3
            """
            return pin.AM_INS, marks[addrMode].index * 3

        # for 暫存器循址
        if addrMode in REGISTERS:
            return pin.AM_REG, REGISTERS[addrMode]

        # for 立即循址
        if re.match(r'^[0-9]+$', addrMode):
            return pin.AM_INS, int(addrMode)

        if re.match(r'^0X[0-9A-F]+$', addrMode):
            return pin.AM_INS, int(addrMode, 16)

        # for 直接循址(10進制)
        match = re.match(r'^\[([0-9]+)\]$', addrMode)
        if match: 
            return pin.AM_DIR, int(match.group(1))

        # for 直接循址(16進制)
        match = re.match(r'^\[(0X[0-9A-F]+)\]$', addrMode)
        if match: 
            return pin.AM_DIR, int(match.group(1), 16)

        # for 寄存器間接循址
        match = re.match(r'^\[(.+)\]$', addrMode)
        if match and match.group(1) in REGISTERS: 
            return pin.AM_RAM, REGISTERS[match.group(1)]

        raise SyntaxError(self)
    
    """
    從 source 取出 op、dst、src
    例如，MOV A, 5，取出 op=MOV, dst=A, src=5
    """
    def prepare_source(self) -> None:
        # 判斷當前代碼(source)是標記字串或是指令字串
        if self.source.endswith(":"):
            self.type = self.TYPE_LABEL
            self.name = self.source.strip(":")
            return

        # 取出 src
        tup = self.source.split(",")
        
        if len(tup) > 2:
            raise SyntaxError(self)

        if len(tup) == 2:
            self.src = tup[1].strip()

        # 取出 dst
        tup = re.split(r" +", tup[0])

        if len(tup) > 2:
            raise SyntaxError(self)

        if len(tup) == 2:
            self.dst = tup[1].strip()

        # 取出 op
        self.op = tup[0].strip()

    """
    將當前的代碼編譯成RAM的值(ir指令的二進制碼)
    """
    def compile_code(self) -> List[int]:
        op = self.get_op()
        amd, dst = self.get_am(self.dst)
        ams, src = self.get_am(self.src)

        # 遇到不支持的語法時，拋出異常
        if src is not None and (amd, ams) not in ASM.INSTRUCTIONS[2][op]:
            raise SyntaxError
        if src is None and dst and amd not in ASM.INSTRUCTIONS[1][op]:
            raise SyntaxError
        if src is None and dst is None and op not in ASM.INSTRUCTIONS[0]:
            raise SyntaxError
        
        amd = amd or 0
        ams = ams or 0
        dst = dst or 0
        src = src or 0

        # 遇到支持的語法
        if op in OP2SET:
            ir = op | (amd << 2 ) | ams
        elif op in OP1SET:
            ir = op | amd
        else:
            ir = op

        return [ir, dst, src]

    def __repr__(self) -> str:
        return f"[{self.number}] - {self.source}"

class SyntaxError(Exception):
    def __init__(self, code: Code, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.code = code

def compile_program() -> None:
    global codes
    global marks

    with open(inputFile, encoding='utf-8') as fr:
        lines = fr.readlines()

    for index, line in enumerate(lines):
        source = line.strip()
        
        # 去除註釋
        if ';' in source:
            match = re_annotation.match(source)
            source = match.group(1)

        # 去除空白行
        if not source: 
            continue
        
        code = Code(index+1, source)
        codes.append(code)

    # 確保若最後一行是標記但沒有代碼，造成的錯誤
    # 若已經有 HLT，多一個HLT並不會造成錯誤
    code = Code(index+2, 'HLT')
    codes.append(code)

    # ==== 處理標記 ====
    # 從 codes移除標記行、記錄標記字串、紀錄標記字串下一行指令的行號

    # 處理標記後的 codes
    result = []

    # 用於紀錄標記的下一行指令
    current = None
    
    # 注意，此處為反向遍歷，
    # 因為要記錄標記行的下一個位置，需要先讓下一行先出現，因此使用反向遍歷
    for var in range(len(codes)-1, -1, -1):
        code:Code = codes[var]

        if code.type == Code.TYPE_CODE:
            # 若 code 為一般指令，不做任何處理，只將指令放回
            current = code
            # 因為是反向遍歷，因此代碼插入第一位，才不會改變原來代碼的順序
            result.insert(0, code)
            continue

        if code.type == Code.TYPE_LABEL:            
            # 若 code 為標記，將標記加入 marks 的變數中
            # 例如，mark[INCREASE] = Code(4, "ADD D, 50")
            # 所以 current 紀錄前一個的指令，因為是反向遍歷，代表標記行的下一個指令

            marks[code.name] = current
            continue
        
        raise SyntaxError(code)

    # 更新每個 Code-Obj 的 index，預設為 0
    # 在 get_am() 中會使用到
    for index, var in enumerate(result):
        var.index = index

    # ==== 寫入 program.bin ====
    with open(outputFile, 'wb') as fw:
        for code in result:
            values = code.compile_code()

            for value in values:
                result = value.to_bytes(1, byteorder="little")
                fw.write(result)

def main():
    try:
        compile_program()
        print("Compile Program.bin finished !!")
    except SyntaxError as e:
        print(f"Syntax error at {e.code}")
        return

if __name__ == "__main__":
    main()