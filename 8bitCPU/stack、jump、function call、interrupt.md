## 堆棧基礎，記憶體管理

- 因此需要對記憶體的存取區域進行分區管理
- 記憶體分層管理

  - 分段管理，將記憶體分為不同的區段

    - 代碼段 (Code-segment, cs)
    - 堆棧段 (Stack-segment, ss)
  - 分頁管理

    分頁管理是分段管理的進階

    分頁管理把程序的邏輯記憶體空間劃分為許多個固定長度的小片，稱為頁(page），

    對應的，將實體記憶體空間也分為許多個固定長度的小部分（稱為幀 frame）

    `每個頁對應一個幀（幀不需要連續）`。 這樣，再記憶體管理時，一個程序最多也就產生最後一頁的一部分的空間浪費

    ![image](doc/stack/stack-2.png)

- `注意`，本專案的記憶體管理和 8086 的方式略有不同

  - 8086:


    | 用途   | 記憶體位址計算方式 |
    | ------ | ------------------ |
    | 程式碼 | (CS << 1) + offset |
    | 堆棧   | (SS << 1) + offset |
  - 本專案:

    本專案的 MSR 和 MAR 共同決定RAM的地址，且使用堆棧時，`將 SS 設置為 1`

    SS 保存 MSR-REG 的值，PC 保存 MAR-REG 的值，`addr = MSR << 2 | MAR`

    例如，SS = 01，PC = 00，Addr = 0x0100 | 0x00 = 0x0100

    可定址的範圍為 0x0100 - 0x01FF = 256 byte
- 相關寄存器

  - SS，保存 MSR 的值，用於設置記憶體的位址為 0x01**
  - SP，指向棧頂的位置，入棧時，SP-1，出棧時，SP+1
  - `注意`，stack 中的資料，是 `從記憶體的尾部開始放置`，

    > sp=0，代表 stack 已滿
    >

    > 利用 sp 可以設置 stack 區域的大小
    >

    ![image](doc/stack/stack-3.png)

## `PUSH`指令

- PUSH 指令的二進制編碼

  ```
  PUSH = (10 << 2) | 0x40 = 0x68
  ```
- PUSH 指令的控制訊號，以 PUSH <寄存器循址> 為例

  ```
  # 資料寫入stack前，先將SP值-1
  SP_OUT | A_IN,  # 將 SP 的值寫入A
  OP_DEC | SP_IN | ALU_OUT, # 將 ALU 設置為-1操作，將A的值-1後，放回SP

  # 將 SP 值傳遞給MSR，將RAM定位到stack區
  SP_OUT | MAR_IN,  # 將SP的值輸出到 MAR
  SS_OUT | MSR_IN,  # 將SS的值輸出到 MSR

  # 將資料寫入 stack後，將RAM定位重新定位到指令區
  DST_R | RAM_IN,  # 將 DST 指向寄存器的值寫入 RAM，將要寫入stack的值寫入RAM的stack區
  CS_OUT | MSR_IN,  # 將 MSR 還原，將CS的值寫入MSR，堆棧操作後，還原成一般代碼的定址
  ```

## `POP`指令

- POP 指令的二進制編碼
  ```
  POP = (11 << 2) | 0x40 = 0x6c
  ```
- POP 指令的控制訊號，以 POP <寄存器循址> 為例
  ```
  # 將 SP 值傳遞給MSR，將RAM定位到stack區
  SP_OUT | MAR_IN,
  SS_OUT | MSR_IN,

  # 將資料從RAM的stack區取出後，寫入DST指向的REG中
  DST_W | RAM_OUT,  

  # 資料出棧後，將 SP+1
  SP_OUT | A_IN,
  OP_INC | SP_IN | ALU_OUT,

  # 將RAM定位回復到指令區
  CS_OUT | MSR_IN,
  ```

## 函數調用的實現

- 基本概念
  - 利用 call指令進行函數調用
  - 執行 call指令時，記錄`下一行指令的位置`
  - 在函數中，利用 ret指令返回調用前的地址，ret指令會將保存的位置寫回PC

## `CALL`指令

- CALL 指令的二進制編碼
  ```
  CALL = (12 << 2) | 40    # 0x7c
  ```
- CALL 指令的控制訊號，以 CALL <立即循址> 為例
  ```
  # 資料寫入stack前，先將SP值-1
  SP_OUT | A_IN,              # 將 SP 的值寫入A
  OP_DEC | SP_IN | ALU_OUT,   # 將 ALU 設置為-1操作，將A的值-1後，放回SP

  # 將 SP 值傳遞給MSR，將RAM定位到stack區
  SP_OUT | MAR_IN,    # 將SP的值輸出到MAR，設定段位址
  SS_OUT | MSR_IN,    # 將SS的值輸出到MSR，設定偏移位址

  # 將函數調用前的位址寫入 stack，確保函數調用能夠後進先出(LIFO)
  # 將函數調用的位置寫入 PC
  # 將RAM定位到指令區  
  PC_OUT | RAM_IN,    # 將當前指令行的PC值寫入RAM的stack區
  DST_OUT | PC_IN,    # 將DST的值寫入PC(將函數要跳轉的位置寫入PC)
  CS_OUT | MSR_IN,    # 將MSR還原，將CS的值寫入MSR(將RAM重新定位回指令區)
  ```

## `RET`指令

- RET 指令的二進制編碼

  ```
  RET = 0x1
  ```
- RET 指令的控制訊號，以 RET <立即循址> 為例

  ```
  # 將 SP 值傳遞給MSR，將RAM定位到stack區
  SP_OUT | MAR_IN,
  SS_OUT | MSR_IN,

  # 將函數調用前的地址，從RAM的stack區取出後，寫入PC中
  RAM_OUT | PC_IN,  

  # 函數調用前的地址出棧後，將 SP+1
  SP_OUT | A_IN,
  OP_INC | SP_IN | ALU_OUT,

  # 將RAM定位回復到指令區
  CS_OUT | MSR_IN,
  ```

## 中斷的實現

- 實現中斷的硬體電路
  ![image](doc/interrupt/interrupt-1.png)

## `INT`指令

- INT 指令的二進制編碼

  ```
  INT = (13 << 2) | 0x40 = 0x74
  ```
- INT 指令的控制訊號，以 INT <立即循址> 為例

  ```
  # 資料寫入stack前，先將SP值-1
  SP_OUT | A_IN,              # 將 SP 的值寫入A
  OP_DEC | SP_IN | ALU_OUT,   # 將 ALU 設置為-1操作，將A的值-1後，放回SP

  # 將 SP 值傳遞給MSR，將RAM定位到stack區
  SP_OUT | MAR_IN,    # 將SP的值輸出到MAR
  SS_OUT | MSR_IN,    # 將SS的值輸出到MSR

  # 將函數調用前的位址寫入 stack，確保函數調用能夠後進先出(LIFO)
  # 將函數調用的位置寫入 PC
  # 將RAM定位到指令區
  # 觸發中斷後，將中斷允許狀態暫時關閉，直到中斷返回
  PC_OUT | RAM_IN,    # 將當前指令行的PC值寫入RAM的stack區
  DST_OUT | PC_IN,    # 將DST的值寫入PC(將函數要跳轉的位置寫入PC)
  CS_OUT | MSR_IN | ALU_PSW | ALU_CLI,    # 將MSR還原，將CS的值寫入MSR(將RAM重新定位回指令區)，PSW狀態輸出，關閉中斷允許狀態
  ```

## `IRET`指令

- IRET 指令的二進制編碼

  ```
  IRET = 0x2
  ```
- IRET 指令的控制訊號，以 IRET <立即循址> 為例

  ```
  # 將 SP 值傳遞給MSR，將RAM定位到stack區
  SP_OUT | MAR_IN,
  SS_OUT | MSR_IN,

  # 將函數調用前的地址，從RAM的stack區取出後，寫入PC中
  PC_IN | RAM_OUT,

  # 函數調用前的地址出棧後，將 SP+1
  SP_OUT | A_IN,
  OP_INC | SP_IN | ALU_OUT,

  # 將RAM定位回復到指令區，開啟PSW輸出，中斷結束後，重新將中斷允許狀態打開
  CS_OUT | MSR_IN | ALU_PSW | ALU_STI,
  ```

## `STI`指令

- STI 指令的二進制編碼

```
STI = 0x3
```
- STI 指令的控制訊號，

```
ALU_PSW | ALU_STI
```

## `CLI`指令

- CLI 指令的二進制編碼

```
CLI = 0x4
```
- CLI 指令的控制訊號，

```
ALU_PSW | ALU_CLI
```
