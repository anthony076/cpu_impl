'''
assembly.py 用於定義指令對應的控制訊號

參考，微程序控制指令基礎 @ instruction.md
參考，8bitCPU.CircuitProject > 00: Main > Main Demo 
'''

from pin_define import pin

# 定義微程序控制指令，FETCH，的控制訊號，用來取得 ir 指令
FETCH = [
    # 將 PC 傳遞給 MAR > 根據 MAR 從 RAM 取操作指令，並將 PC+1 
    pin.PC_OUT | pin.MAR_IN,                # 0x8040  : 0b1000000001000000

    pin.RAM_OUT | pin.IR_IN | pin.PC_INC,   # 0x1c0a4 : 0b11100000010100100

    # 將 PC 傳遞給 MAR > 根據 MAR 從 RAM 取DST，並將 PC+1
    pin.PC_OUT | pin.MAR_IN,                # 0x8040  : 0b1000000001000000
    pin.RAM_OUT | pin.DST_IN | pin.PC_INC,  # 0x1c0c4 : 0b11100000011000100
    
    # 將 PC 傳遞給 MAR > 根據 MAR 從 RAM 取SRC，並將 PC+1
    pin.PC_OUT | pin.MAR_IN,                # 0x8040  : 0b1000000001000000
    pin.RAM_OUT | pin.SRC_IN | pin.PC_INC,  # 0x1c0e4 : 0b11100000011100100
]

# ==== 建立2地址指令的二進制代碼 ====
MOV = (0 << pin.ADDR2_SHIFT) | pin.ADDR2    # 0x80
ADD = (1 << pin.ADDR2_SHIFT) | pin.ADDR2    # 0x90
SUB = (2 << pin.ADDR2_SHIFT) | pin.ADDR2    # 0xa0
CMP = (3 << pin.ADDR2_SHIFT) | pin.ADDR2    # 0xb0
AND = (4 << pin.ADDR2_SHIFT) | pin.ADDR2    # 0xc0
OR = (5 << pin.ADDR2_SHIFT) | pin.ADDR2    # 0xd0
XOR = (6 << pin.ADDR2_SHIFT) | pin.ADDR2    # 0xe0

# ==== 建立1地址指令的二進制代碼 ====
INC = (0 << pin.ADDR1_SHIFT) | pin.ADDR1    # 0x40
DEC = (1 << pin.ADDR1_SHIFT) | pin.ADDR1    # 0x44
NOT = (2 << pin.ADDR1_SHIFT) | pin.ADDR1    # 0x48
JMP = (3 << pin.ADDR1_SHIFT) | pin.ADDR1    # 0x4c

# 以下跳轉命令，根據PSW的狀態進行跳轉，
# 有三種狀態，溢位、零、奇偶，每個狀態有兩種跳轉方式

# 溢位跳轉
JO = (4 << pin.ADDR1_SHIFT) | pin.ADDR1    # 0x50
# 非溢位跳轉
JNO = (5 << pin.ADDR1_SHIFT) | pin.ADDR1    # 0x54
# 零跳轉
JZ = (6 << pin.ADDR1_SHIFT) | pin.ADDR1    # 0x58
# 非零跳轉
JNZ = (7 << pin.ADDR1_SHIFT) | pin.ADDR1    # 0x5c
# 奇跳轉
JP = (8 << pin.ADDR1_SHIFT) | pin.ADDR1    # 0x60
# 偶跳轉
JNP = (9 << pin.ADDR1_SHIFT) | pin.ADDR1    # 0x64

# 將資料放入 stack 區
PUSH = (10 << pin.ADDR1_SHIFT) | pin.ADDR1   # 0x68
# 將資料從 stack 區取出
POP = (11 << pin.ADDR1_SHIFT) | pin.ADDR1    # 0x6c

CALL =  (12 << pin.ADDR1_SHIFT) | pin.ADDR1    # 0x70
INT = (13 << pin.ADDR1_SHIFT) | pin.ADDR1    # 0x74

# ==== 建立0地址指令的二進制代碼 ====
# 定義 ir 指令，HLT (0 地址指令)
#   0地址指令 = 00xxxxxx，
#   HLT 指令，令所有的 x 皆為 1111
HLT = 0x3f
NOP = 0
RET = 1
IRET = 2
STI = 3 # 開啟中斷
CLI = 4 # 關閉中斷

# 依照操作數、指令名、循址方式，對指令進行分類，用於判斷指令類型
# INSTRUCTIONS 用於編譯組語時，對語法類型進行判斷後，產生對應的控制訊號
INSTRUCTIONS = {
    # 限定2地址指令
    2: {
        MOV : { # 限定 MOV 指令，

            # MOV 操作模式1，限定 MOV <寄存器循址> <立即循址>，例如，MOV A, 5;
            #       (輸出)pin.DST_W，設置 DW = 1，將BUS上的值寫入DST-REG指向的暫存器
            #       (輸入)pin.SRC_OUT，將 SRC-REG 的值寫入BUS
            (pin.AM_REG, pin.AM_INS) : [
                pin.SRC_OUT | pin.DST_W     # 對應的控制訊號 0x7 | 0x2000 = 0x2007
            ],

            # MOV 操作模式2，限定 MOV <寄存器循址> <寄存器循址>，例如，MOV A, 5; MOV D, A;
            #       (輸出)pin.SRC_R，設置 SR = 1，將 SRC-REG 指向的暫存器的內容，寫入 BUS
            #       (輸入)pin.DST_W，設置 DW = 1，將 BUS 上的值寫入 DST-REG 指向的暫存器
            (pin.AM_REG, pin.AM_REG) : [
                pin.SRC_R | pin.DST_W       # 對應的控制訊號 0x400 | 0x2000 = 0x2400
            ],

            # MOV 操作模式3，限定 MOV <寄存器循址> <直接循址>，例如，MOV D, [5];
            #       (輸出)pin.SRC_OUT，將 SRC-REG 的值寫入BUS
            #       (輸入)pin.MAR_IN，將 BUS 上的值寫入 MAR

            #       (輸出)pin.RAM_OUT，將 RAM 的值寫入BUS
            #       (輸入)pin.DST_W，設置 DW = 1，將BUS上的值寫入DST-REG指向的暫存器
            (pin.AM_REG, pin.AM_DIR) : [
                pin.SRC_OUT | pin.MAR_IN,   # 對應的控制訊號 0x7 | 0x40 = 0x47
                pin.RAM_OUT | pin.DST_W     # 對應的控制訊號 0x4 | 0x2000 = 0x2004
            ],

            # MOV 操作模式4，限定 MOV <寄存器循址> <寄存器間接循址>，例如，MOV A, 5; MOV D, [A];
            #       (輸出)pin.SRC_R，設置 SR = 1，將 SRC-REG 指向的暫存器的內容，寫入 BUS
            #       (輸入)pin.MAR_IN，將 BUS 上的值寫入 MAR

            #       (輸出)pin.RAM_OUT，將 RAM 的值寫入BUS
            #       (輸入)pin.DST_W，設置 DW = 1，將BUS上的值寫入DST-REG指向的暫存器
            (pin.AM_REG, pin.AM_RAM) : [
                pin.SRC_R | pin.MAR_IN,     # 對應的控制訊號 0x400 | 0x40 = 0x440
                pin.DST_W | pin.RAM_OUT     # 對應的控制訊號 0x2000 | 0x4 = 0x2004
            ],

            # MOV 操作模式5，限定 MOV <直接循址> <立即循址>，例如，MOV [0X2f], 5
            #       (輸出)pin.DST_OUT，將 DST-REG 的值寫入BUS
            #       (輸入)pin.MAR_IN，將 BUS 上的值寫入 MAR

            #       (輸出)pin.SRC_OUT，將 SRC-REG 的值寫入BUS
            #       (輸入)pin.RAM_IN，將 BUS 上的值寫入 RAM
            (pin.AM_DIR, pin.AM_INS) : [
                pin.DST_OUT | pin.MAR_IN,   # 對應的控制訊號 0x6 | 0x40 = 0x46
                pin.SRC_OUT | pin.RAM_IN,   # 對應的控制訊號 0x7 | 0x80 = 0x87
            ],

            # MOV 操作模式6，限定 MOV <直接循址> <寄存器循址>，例如，MOV C, 0x18; MOV [0x2f], C;
            #       (輸出)pin.DST_OUT，將 DST-REG 的值寫入BUS
            #       (輸入)pin.MAR_IN，將 BUS 上的值寫入 MAR

            #       (輸出)pin.SRC_R，設置 SR = 1，將 SRC-REG 指向的暫存器的內容，寫入 BUS
            #       (輸入)pin.RAM_IN，將 BUS 上的值寫入 RAM
            (pin.AM_DIR, pin.AM_REG) : [
                pin.DST_OUT | pin.MAR_IN,   # 對應的控制訊號 0x6 | 0x40 = 0x46
                pin.SRC_R | pin.RAM_IN,     # 對應的控制訊號 0x400 | 0x80 = 0x480
            ],

            # MOV 操作模式7，限定 MOV <直接循址> <直接循址>，例如，MOV [0X2e], 18; MOV [0X2f], [0X2e];
            #       (輸出)pin.SRC_OUT，將 SRC-REG 的值寫入BUS
            #       (輸入)pin.MAR_IN，將 BUS 上的值寫入 MAR

            #       (輸出)pin.RAM_OUT，將 RAM 的值寫入BUS
            #       (輸入)pin.T1_IN，BUS 上的值寫入 T1-REG

            #       (輸出)pin.DST_OUT，將 DST-REG 的值寫入BUS
            #       (輸入)pin.MAR_IN，將 BUS 上的值寫入 MAR

            #       (輸出)pin.T1_OUT，將 T1-REG 的值寫入BUS
            #       (輸入)pin.RAM_IN，將 BUS 上的值寫入 RAM
            (pin.AM_DIR, pin.AM_DIR) : [
                pin.SRC_OUT | pin.MAR_IN,   # 對應的控制訊號，0x7 | 0x40 = 0x47
                pin.RAM_OUT | pin.T1_IN,    # 對應的控制訊號，0x4 | 0x2a0 = 0x2a4
                pin.DST_OUT | pin.MAR_IN,   # 對應的控制訊號，0x6 | 0x40 = 0x46
                pin.T1_OUT | pin.RAM_IN,    # 對應的控制訊號，0x15 | 0x80 = 0x95
            ],

            # MOV 操作模式8，限定 MOV <直接循址> <寄存器間接循址>，例如，MOV [0X18], 0xfe; MOV C, 0X18; MOV [0X2f], [C];
            #       (輸出)pin.SRC_R，設置 SR = 1，將 SRC-REG 指向的暫存器的內容，寫入 BUS
            #       (輸入)pin.MAR_IN，將 BUS 上的值寫入 MAR

            #       (輸出)pin.RAM_OUT，將 RAM 的值寫入BUS
            #       (輸入)pin.T1_IN，BUS 上的值寫入 T1-REG

            #       (輸出)pin.DST_OUT，將 DST-REG 的值寫入BUS
            #       (輸入)pin.MAR_IN，將 BUS 上的值寫入 MAR

            #       (輸出)pin.T1_OUT，將 T1-REG 的值寫入BUS
            #       (輸入)pin.RAM_IN，將 BUS 上的值寫入 RAM
            (pin.AM_DIR, pin.AM_RAM) : [
                pin.SRC_R | pin.MAR_IN,
                pin.RAM_OUT | pin.T1_IN,
                pin.DST_OUT | pin.MAR_IN,
                pin.T1_OUT | pin.RAM_IN,
            ],

            # MOV 操作模式9，限定 MOV <寄存器間接循址> <立即循址>，例如，MOV C, 0x18; MOV [C], 5;
            (pin.AM_RAM, pin.AM_INS) : [
                pin.DST_R | pin.MAR_IN,
                pin.SRC_OUT | pin.RAM_IN,
            ],

            # MOV 操作模式10，限定 MOV <寄存器間接循址> <寄存器循址>，例如，MOV C, 0x18; MOV D, 0x33; MOV [C], D;
            (pin.AM_RAM, pin.AM_REG) : [
                pin.DST_R | pin.MAR_IN,
                pin.SRC_R | pin.RAM_IN,
            ],

            # MOV 操作模式11，限定 MOV <寄存器間接循址> <直接接循址>，例如，MOV [0X30], 0xdd; MOV C, 0x18; MOV [C], [0X30];
            (pin.AM_RAM, pin.AM_DIR) : [    
                pin.SRC_OUT | pin.MAR_IN,
                pin.RAM_OUT | pin.T1_IN,
                pin.DST_R | pin.MAR_IN,
                pin.T1_OUT | pin.RAM_IN,
            ],

            # MOV 操作模式12，限定 MOV <寄存器間接循址> <寄存器間接循址>，例如，MOV [0X30], 0xee; MOV D, 0x30; MOV C, 0x18; MOV [C], [D];
            (pin.AM_RAM, pin.AM_RAM) : [    
                pin.SRC_R | pin.MAR_IN,
                pin.RAM_OUT | pin.T1_IN,
                pin.DST_R | pin.MAR_IN,
                pin.T1_OUT | pin.RAM_IN,
            ],
        },

        # 設置 ADD指令的微程序控制訊號
        ADD: {
            # pin.DST_R | pin.A_IN，將 DST-REG 指向的REG的內容寫入BUS，將 BUS的值寫入 A-REG
            # pin.SRC_OUT | pin.B_IN，將 SRC-REG的內容寫入BUS，將 BUS的值 寫入 B-REG
            # pin.OP_ADD | pin.ALU_OUT | pin.DST_W | pin.ALU_PSW，ALU 執行加法，將ALU的結果輸出到 BUS，將 BUS上的值寫入DST指向的暫存器，致能ALU和 PSW 的輸出
            (pin.AM_REG, pin.AM_INS) : [    #設置 ADD指令的循址模式為，ADD <REG循址> <立即數循址>，例如，ADD D, 5
                pin.DST_R | pin.A_IN,
                pin.SRC_OUT | pin.B_IN,
                pin.OP_ADD | pin.ALU_OUT | pin.DST_W | pin.ALU_PSW, 
            ],
            
            # pin.DST_R | pin.A_IN，將 DST-REG 指向的REG的內容寫入BUS，將 BUS的值寫入 A-REG
            # pin.SRC_R | pin.B_IN，將 SRC-REG 指向的REG的內容寫入BUS，將 BUS的值 寫入 B-REG
            # pin.OP_ADD | pin.ALU_OUT | pin.DST_W | pin.ALU_PSW，ALU 執行加法，將ALU的結果輸出到 BUS，將 BUS上的值寫入DST指向的暫存器，致能ALU和 PSW 的輸出
            (pin.AM_REG, pin.AM_REG) : [    #設置 ADD指令的循址模式為，ADD <REG循址> <REG循址>，例如，MOV D, 4; MOV C, 7; ADD D, C;
                pin.DST_R | pin.A_IN,
                pin.SRC_R | pin.B_IN,
                pin.OP_ADD | pin.ALU_OUT | pin.DST_W | pin.ALU_PSW, 
            ],
        },

        # 設置SUB指令的微程序控制訊號
        SUB: {
            # pin.DST_R | pin.A_IN，將 DST-REG 指向的REG的內容寫入BUS，將 BUS的值寫入 A-REG
            # pin.SRC_OUT | pin.B_IN，將 SRC-REG的內容寫入BUS，將 BUS的值 寫入 B-REG
            # pin.OP_SUB | pin.ALU_OUT | pin.DST_W | pin.ALU_PSW，ALU 執行減法，將ALU的結果輸出到 BUS，將 BUS上的值寫入DST指向的暫存器，致能ALU和 PSW 的輸出
            (pin.AM_REG, pin.AM_INS) : [    #設置 ADD指令的循址模式為，SUB <REG循址> <立即數循址>，例如，MOV D, 5; SUB D, 5;
                pin.DST_R | pin.A_IN,
                pin.SRC_OUT | pin.B_IN,
                pin.OP_SUB | pin.ALU_OUT | pin.DST_W | pin.ALU_PSW, 
            ],
            
            # pin.DST_R | pin.A_IN，將 DST-REG 指向的REG的內容寫入BUS，將 BUS的值寫入 A-REG
            # pin.SRC_R | pin.B_IN，將 SRC-REG 指向的REG的內容寫入BUS，將 BUS的值 寫入 B-REG
            # pin.OP_SUB | pin.ALU_OUT | pin.DST_W | pin.ALU_PSW，ALU 執行減法，將ALU的結果輸出到 BUS，將 BUS上的值寫入DST指向的暫存器，致能ALU和 PSW 的輸出
            (pin.AM_REG, pin.AM_REG) : [    #設置 ADD指令的循址模式為，SUB <REG循址> <REG循址>，例如，MOV D, 5; MOV C, 5; SUB D, C;
                pin.DST_R | pin.A_IN,
                pin.SRC_R | pin.B_IN,
                pin.OP_SUB | pin.ALU_OUT | pin.DST_W | pin.ALU_PSW, 
            ],
        },
        
        CMP: {
            (pin.AM_REG, pin.AM_INS) : [    #設置 CMP指令的循址模式為，CMP <REG循址> <立即循址>，
                pin.DST_R | pin.A_IN,
                pin.SRC_OUT | pin.B_IN,
                # CMP 指令是利用減法指令實現，
                pin.OP_SUB | pin.ALU_PSW, 
            ],
            
            (pin.AM_REG, pin.AM_REG) : [    #設置 CMP指令的循址模式為，CMP <REG循址> <REG循址>，例如，MOV C, 5; MOV D, 5; CMP C, D;
                pin.DST_R | pin.A_IN,
                pin.SRC_R | pin.B_IN,
                # CMP 指令是利用減法指令實現，
                pin.OP_SUB| pin.ALU_PSW, 
            ],
        },

        AND : {
            (pin.AM_REG, pin.AM_INS) : [    #設置 AND指令的循址模式為，AND <REG循址> <立即循址>，
                pin.DST_R | pin.A_IN,
                pin.SRC_OUT | pin.B_IN,
                pin.OP_AND | pin.ALU_OUT | pin.DST_W | pin.ALU_PSW, 
            ],
            
            (pin.AM_REG, pin.AM_REG) : [    #設置 AND指令的循址模式為，AND <REG循址> <REG循址>，例如，MOV C, 5; MOV D, 2; AND D, C;
                pin.DST_R | pin.A_IN,
                pin.SRC_R | pin.B_IN,
                pin.OP_AND | pin.ALU_OUT | pin.DST_W | pin.ALU_PSW, 
            ],
        },

        OR : {
            (pin.AM_REG, pin.AM_INS) : [    #設置 OR指令的循址模式為，OR <REG循址> <立即循址>，
                pin.DST_R | pin.A_IN,
                pin.SRC_OUT | pin.B_IN,
                pin.OP_OR | pin.ALU_OUT | pin.DST_W | pin.ALU_PSW, 
            ],
            
            (pin.AM_REG, pin.AM_REG) : [    #設置 OR指令的循址模式為，OR <REG循址> <REG循址>，例如，MOV C, 5; MOV D, 2; OR D, C;
                pin.DST_R | pin.A_IN,
                pin.SRC_R | pin.B_IN,
                pin.OP_OR | pin.ALU_OUT | pin.DST_W | pin.ALU_PSW, 
            ],
        },

        XOR : {
            (pin.AM_REG, pin.AM_INS) : [    #設置 XOR指令的循址模式為，XOR <REG循址> <立即循址>，
                pin.DST_R | pin.A_IN,
                pin.SRC_OUT | pin.B_IN,
                pin.OP_XOR | pin.ALU_OUT | pin.DST_W | pin.ALU_PSW, 
            ],
            
            (pin.AM_REG, pin.AM_REG) : [    #設置 XOR指令的循址模式為，XOR <REG循址> <REG循址>，例如，MOV C, 7; MOV D, 2; XOR D, C;
                pin.DST_R | pin.A_IN,
                pin.SRC_R | pin.B_IN,
                pin.OP_XOR | pin.ALU_OUT | pin.DST_W | pin.ALU_PSW, 
            ],
        },
    },
    
    # 限定1地址指令
    1: {
        INC: {
            pin.AM_REG : [  #設置 INC指令的循址模式為，INC <REG循址>，
                pin.DST_R | pin.A_IN,
                pin.OP_INC | pin.ALU_OUT | pin.DST_W | pin.ALU_PSW, 
            ],
        },
        DEC: {
            pin.AM_REG : [  #設置 DEC指令的循址模式為，DEC <REG循址>，
                pin.DST_R | pin.A_IN,
                pin.OP_DEC | pin.ALU_OUT | pin.DST_W | pin.ALU_PSW, 
            ],
        },
        NOT: {
            pin.AM_REG : [  #設置 NOT指令的循址模式為，NOT <REG循址>，
                pin.DST_R | pin.A_IN,
                pin.OP_NOT | pin.ALU_OUT | pin.DST_W | pin.ALU_PSW, 
            ],
        },
        JMP: {
            pin.AM_INS : [  #設置 JMP指令的循址模式為，JMP <立即循址>，
                pin.DST_OUT | pin.PC_IN,    # 控制訊號為 0xc006
            ],
        },
        JO: {
            pin.AM_INS : [  #設置 JO指令的循址模式為，JO <立即循址>，
                pin.DST_OUT | pin.PC_IN,    
            ],
        },
        JNO: {
            pin.AM_INS : [  #設置 JNO指令的循址模式為，JNO <立即循址>，
                pin.DST_OUT | pin.PC_IN,    
            ],
        },
        JZ: {
            pin.AM_INS : [  #設置 JZ指令的循址模式為，JZ <立即循址>，
                pin.DST_OUT | pin.PC_IN,    
            ],
        },
        JNZ: {
            pin.AM_INS : [  #設置 JNZ指令的循址模式為，JNZ <立即循址>，
                pin.DST_OUT | pin.PC_IN,    
            ],
        },
        JP: {
            pin.AM_INS : [  #設置 JP指令的循址模式為，JP <立即循址>，
                pin.DST_OUT | pin.PC_IN,    
            ],
        },
        JNP: {
            pin.AM_INS : [  #設置 JNP指令的循址模式為，JNP <立即循址>，
                pin.DST_OUT | pin.PC_IN,    
            ],
        },
        PUSH: {
            pin.AM_INS : [  #設置 PUSH指令的循址模式為，PUSH <立即循址>，
                pin.SP_OUT | pin.A_IN,
                pin.OP_DEC | pin.SP_IN | pin.ALU_OUT,
                pin.SP_OUT | pin.MAR_IN,
                pin.SS_OUT | pin.MSR_IN,
                pin.DST_OUT | pin.RAM_IN,
                pin.CS_OUT | pin.MSR_IN,
            ],

            pin.AM_REG : [  #設置 PUSH指令的循址模式為，PUSH <REG循址>，
                # 資料寫入stack前，先將SP值-1
                pin.SP_OUT | pin.A_IN,      # 將 SP 的值寫入A
                pin.OP_DEC | pin.SP_IN | pin.ALU_OUT, # 將 ALU 設置為-1操作，將A的值-1後，放回SP

                # 將 SP 值傳遞給MSR，將RAM定位到stack區
                pin.SP_OUT | pin.MAR_IN,    # 將SP的值輸出到MAR
                pin.SS_OUT | pin.MSR_IN,    # 將SS的值輸出到MSR

                # 將資料寫入 stack後，將RAM定位重新定位到指令區
                pin.DST_R | pin.RAM_IN,     # 將 DST 指向寄存器的值寫入 RAM，將要寫入stack的值寫入RAM的stack區
                pin.CS_OUT | pin.MSR_IN,    # 將 MSR 還原，將CS的值寫入MSR，，堆棧操作後，還原成一般代碼的定址
            ],
        },
        POP: {
            pin.AM_REG : [  #設置 POP指令的循址模式為，POP <REG循址>，
                # 將 SP 值傳遞給MSR，將RAM定位到stack區
                pin.SP_OUT | pin.MAR_IN,
                pin.SS_OUT | pin.MSR_IN,

                # 將資料從RAM的stack區取出後，寫入DST指向的REG中
                pin.DST_W | pin.RAM_OUT,    

                # 資料出棧後，將 SP+1
                pin.SP_OUT | pin.A_IN,
                pin.OP_INC | pin.SP_IN | pin.ALU_OUT,

                # 將RAM定位回復到指令區
                pin.CS_OUT | pin.MSR_IN,
            ],
        },
        CALL: {
            pin.AM_INS : [  #設置 CALL指令的循址模式為，CALL <立即循址>，
                # 資料寫入stack前，先將SP值-1
                pin.SP_OUT | pin.A_IN,                  # 將 SP 的值寫入A
                pin.OP_DEC | pin.SP_IN | pin.ALU_OUT,   # 將 ALU 設置為-1操作，將A的值-1後，放回SP
                
                # 將 SP 值傳遞給MSR，將RAM定位到stack區
                pin.SP_OUT | pin.MAR_IN,    # 將SP的值輸出到MAR
                pin.SS_OUT | pin.MSR_IN,    # 將SS的值輸出到MSR
                
                # 將函數調用前的位址寫入 stack，確保函數調用能夠後進先出(LIFO)
                # 將函數調用的位置寫入 PC
                # 將RAM定位到指令區                
                pin.PC_OUT | pin.RAM_IN,    # 將當前指令行的PC值寫入RAM的stack區
                pin.DST_OUT | pin.PC_IN,    # 將DST的值寫入PC(將函數要跳轉的位置寫入PC)
                pin.CS_OUT | pin.MSR_IN,    # 將MSR還原，將CS的值寫入MSR(將RAM重新定位回指令區)
            ],

            pin.AM_REG : [  #設置 CALL指令的循址模式為，CALL <REG循址>，
                # 資料寫入stack前，先將SP值-1
                pin.SP_OUT | pin.A_IN,                # 將 SP 的值寫入A
                pin.OP_DEC | pin.SP_IN | pin.ALU_OUT, # 將 ALU 設置為-1操作，將A的值-1後，放回SP

                # 將 SP 值傳遞給MSR，將RAM定位到stack區
                pin.SP_OUT | pin.MAR_IN,    # 將SP的值輸出到MAR
                pin.SS_OUT | pin.MSR_IN,    # 將SS的值輸出到MSR

                # 將函數調用前的位址寫入 stack，確保函數調用能夠後進先出(LIFO)
                # 將函數調用的位置寫入 PC
                # 將RAM定位到指令區
                pin.PC_OUT | pin.RAM_IN,    # 將當前指令行的PC值寫入RAM的stack區
                pin.DST_R | pin.PC_IN,      # 將DST指向的REG的值寫入PC(將函數要跳轉的位置寫入PC)
                pin.CS_OUT | pin.MSR_IN,    # 將MSR還原，將CS的值寫入MSR(將RAM重新定位回指令區)
            ],
        },
        INT: {
            pin.AM_INS : [  #設置 INT指令的循址模式為，INT <立即循址>，
                # 資料寫入stack前，先將SP值-1
                pin.SP_OUT | pin.A_IN,                  # 將 SP 的值寫入A
                pin.OP_DEC | pin.SP_IN | pin.ALU_OUT,   # 將 ALU 設置為-1操作，將A的值-1後，放回SP
                
                # 將 SP 值傳遞給MSR，將RAM定位到stack區
                pin.SP_OUT | pin.MAR_IN,    # 將SP的值輸出到MAR
                pin.SS_OUT | pin.MSR_IN,    # 將SS的值輸出到MSR
                
                # 將函數調用前的位址寫入 stack，確保函數調用能夠後進先出(LIFO)
                # 將函數調用的位置寫入 PC
                # 將RAM定位到指令區
                # 觸發中斷後，將中斷允許狀態暫時關閉，直到中斷返回
                pin.PC_OUT | pin.RAM_IN,    # 將當前指令行的PC值寫入RAM的stack區
                pin.DST_OUT | pin.PC_IN,    # 將DST的值寫入PC(將函數要跳轉的位置寫入PC)
                pin.CS_OUT | pin.MSR_IN | pin.ALU_PSW | pin.ALU_CLI,    # 將MSR還原，將CS的值寫入MSR(將RAM重新定位回指令區)，PSW狀態輸出，關閉中斷允許狀態
            ],

            pin.AM_REG : [  #設置 INT指令的循址模式為，INT <REG循址>，
                # 資料寫入stack前，先將SP值-1
                pin.SP_OUT | pin.A_IN,                # 將 SP 的值寫入A
                pin.OP_DEC | pin.SP_IN | pin.ALU_OUT, # 將 ALU 設置為-1操作，將A的值-1後，放回SP

                # 將 SP 值傳遞給MSR，將RAM定位到stack區
                pin.SP_OUT | pin.MAR_IN,    # 將SP的值輸出到MAR
                pin.SS_OUT | pin.MSR_IN,    # 將SS的值輸出到MSR

                # 將函數調用前的位址寫入 stack，確保函數調用能夠後進先出(LIFO)
                # 將函數調用的位置寫入 PC
                # 將RAM定位到指令區
                # 觸發中斷後，將中斷允許狀態暫時關閉，直到中斷返回
                pin.PC_OUT | pin.RAM_IN,    # 將當前指令行的PC值寫入RAM的stack區
                pin.DST_R | pin.PC_IN,      # 將DST指向的REG的值寫入PC(將函數要跳轉的位置寫入PC)
                pin.CS_OUT | pin.MSR_IN | pin.ALU_PSW | pin.ALU_CLI,    # 將MSR還原，將CS的值寫入MSR(將RAM重新定位回指令區)，PSW狀態輸出，關閉中斷允許狀態
            ],
        },
    },
    
    # 限定0地址指令
    0: {
        NOP : [pin.CYC],    # do nothing，直接將CU內部的微程序控制週期計數器設置為0
        HLT : [pin.HLT],
        RET: [
               # 將 SP 值傳遞給MSR，將RAM定位到stack區
                pin.SP_OUT | pin.MAR_IN,
                pin.SS_OUT | pin.MSR_IN,

                # 將函數調用前的地址，從RAM的stack區取出後，寫入PC中
                pin.PC_IN | pin.RAM_OUT,

                # 函數調用前的地址出棧後，將 SP+1
                pin.SP_OUT | pin.A_IN,
                pin.OP_INC | pin.SP_IN | pin.ALU_OUT,

                # 將RAM定位回復到指令區
                pin.CS_OUT | pin.MSR_IN,
        ],
        IRET: [
               # 將 SP 值傳遞給MSR，將RAM定位到stack區
                pin.SP_OUT | pin.MAR_IN,
                pin.SS_OUT | pin.MSR_IN,

                # 將函數調用前的地址，從RAM的stack區取出後，寫入PC中
                pin.PC_IN | pin.RAM_OUT,

                # 函數調用前的地址出棧後，將 SP+1
                pin.SP_OUT | pin.A_IN,
                pin.OP_INC | pin.SP_IN | pin.ALU_OUT,

                # 將RAM定位回復到指令區，開啟PSW輸出，
                # 中斷結束後，重新將中斷允許狀態打開
                pin.CS_OUT | pin.MSR_IN | pin.ALU_PSW | pin.ALU_STI,
        ],
        STI: [
            pin.ALU_PSW | pin.ALU_STI
        ],
        CLI: [
            pin.ALU_PSW | pin.ALU_CLI
        ],
    },
}